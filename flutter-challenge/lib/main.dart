import 'dart:math';
import 'package:challenge/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'number.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  String title;

  MyHomePage({Key? key, required this.title}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return GetX(
      builder: (NumbserController controller) {
        return Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Sum of the numbers " + controller.strNumbers.value + " are:",
                ),
                Text(
                  controller.sum.toString(),
                  style: Theme.of(context).textTheme.headline4,
                ),
              ],
            ),
          ),
          floatingActionButton: Row(
            children: [
              FloatingActionButton(
                onPressed: () => controller.decrement(),
                tooltip: 'decremnet',
                child: Icon(Icons.remove),
              ),
              FloatingActionButton(
                onPressed: () => controller.addRandamNumber(),
                tooltip: 'Increment',
                child: Icon(Icons.add),
              ),
            ],
          ),
        );
      },
      init: NumbserController(),
    );
  }
}
